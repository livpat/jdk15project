#!/usr/bin/env groovy
pipeline {

  agent {
    docker {
      image 'maven:3.6.3-adoptopenjdk-15'
      args '--memory=1g -e JAVA_OPTS=\'-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap\' --name=jdk15Project.$BRANCH_NAME.$BUILD_ID.test -e DOCKER_HOST=$DOCKER_HOST -e DOCKER_TLS_VERIFY=$DOCKER_TLS_VERIFY -e DOCKER_CERT_PATH=$DOCKER_CERT_PATH -v $DOCKER_CERT_PATH:$DOCKER_CERT_PATH'
    }
  }

  tools {
    maven '3.6'
  }

  environment {
    //Use Pipeline Utility Steps plugin to read information from pom.xml into env variables
    def pom = readMavenPom()
    IMAGE = pom.getArtifactId()
    VERSION = pom.getVersion()
    BASE_JAVA_OPTS="-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"
    BASE_CONTAINER_NAME = "$IMAGE.$BRANCH_NAME.$BUILD_ID"
    BASE_NETWORK_NAME = "$BASE_CONTAINER_NAME-network"
    DOCKER_ACTIVEMQ_NAME = "$BASE_CONTAINER_NAME-activemq"
    DOCKER_KAFKA_NAME = "$BASE_CONTAINER_NAME-kafka"
    DOCKER_ZOOKEEPER_NAME = "$BASE_CONTAINER_NAME-zookeeper"
  }

  stages {

    stage('Clean') {
      steps {
        bitbucketStatusNotify(buildState: 'INPROGRESS')
        sh "mvn -T 1C clean -U"
      }
    }

    stage('Compile') {
      steps {
        sh "mvn -T 1C compile"
      }
    }

    stage('Test Compile') {
      steps {
        sh "mvn -T 1C test-compile"
      }
    }

    stage('Unit Test') {
      steps {
        script {
          sh "mvn verify -Dmaven.test.failure.ignore=true"
        }
      }
    }

    stage('SonarQube Analysis') {
        steps {
            script {
                try {
                    withSonarQubeEnv('SonarCloud') {
                        if (env.CHANGE_TARGET) { // if it is a PR
                            sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.8.0.2131:sonar -Dsonar.coverage.jacoco.xmlReportPaths=target/coverage-reports/jacoco.xml -Dsonar.pullrequest.key=$CHANGE_ID -Dsonar.pullrequest.branch=$CHANGE_BRANCH -Dsonar.pullrequest.base=$CHANGE_TARGET'
                        } else {
                            sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.8.0.2131:sonar -Dsonar.coverage.jacoco.xmlReportPaths=target/coverage-reports/jacoco.xml -Dsonar.branch.name=$BRANCH_NAME'
                        }
                    }
                } catch (ex) {
                  unstable('SonarQube Analysis failed!')
                }
            }
        }
    }

  }

  post {
    success {
      bitbucketStatusNotify(buildState: 'SUCCESSFUL')
    }
    failure {
      bitbucketStatusNotify(buildState: 'FAILED')
    }
    unstable {
      bitbucketStatusNotify(buildState: 'FAILED')
    }
    changed {
      echo 'changed'
    }
  }

}
